const fetch = require('node-fetch')
let counter = 0;
setInterval(()=>{
  fetch('http://127.0.0.1:3000', {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    headers: {
        "Content-Type": "application/json",
    },
    body: JSON.stringify({temp: Math.floor(Math.random()*100)}),
  }).then((res)=>res.text())
  .then(res => {console.log(res, counter++)})
  .catch(err => {
    console.log(err)
  })
}, 100)
