const fetch = require('node-fetch')
const _ = require('lodash');
const debug = require('debug')('geoduck');

module.exports = class Queue {
  constructor(getFromDbById, markAsUploadedInDb, farmOSURI, timeoutInterval){
    this.queue = []
    this.getFromDbById = getFromDbById
    this.markAsUploadedInDb = markAsUploadedInDb
    this.locked = false
    this.farmOSURI = farmOSURI
    this.timeoutInterval = timeoutInterval*1000 || 30000
  }
  push(item){
    debug("RECEIVED:", item)
    this.queue.push(item)
    debug("QUEUE AFTER RECEIVING:", this.queue)
    if(!this.locked){
      this._postNext()
    }
  }

  _postNext(){
    debug("QUEUE BEFORE POSTING:", this.queue);

    if(this.queue.length === 0) return;

    const id = this.queue[0];
    const entry = this.getFromDbById(id)
    this.locked = true

    delete entry.id
    delete entry.uploaded

    debug("POSTING:", id)
    debug("DATA:", entry)

    fetch(this.farmOSURI, {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      headers: {
          "Content-Type": "application/json",
      },
      timeout: this.timeoutInterval,
      body: JSON.stringify(entry),
    })
    .then((res)=>{
      if(!res.ok){
        throw new Error('Network response was not ok.')
      }
      debug("POST SUCCEEDED FOR", id);

      // _.remove(this.queue, id)
      this.queue.shift()

      this.markAsUploadedInDb(id)
      if(this.queue.length > 0 ){
        this._postNext()
      }else{
        this.locked = false
      }
    }).catch((err)=>{
      console.error(err)
      // TODO: if the last request never errors after connectivity is lost + another request does not come in, it doesn't keep trying
      setTimeout(this._postNext.bind(this), 5000);
    })
  }
}
